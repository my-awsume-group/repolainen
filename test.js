const supertest = require('supertest');
const server = require('./index');
const chai = require('chai');

chai.should();

const api = supertest.agent(server);

describe('Validate name', () => {
  it('should return: true', (done) => {
      api.post('/api/name')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "Henriikka Jantunen"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(true);
              done();
          });
  });
})

describe('Validate name', () => {
  it('should return: false', (done) => {
      api.post('/api/name')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "Pekka123"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(false);
              done();
          });
  });
})

describe('Validate password', () => {
  it('should return: true', (done) => {
      api.post('/api/password')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "A1qwfnaoes5f-"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(true);
              done();
          });
  });
})

describe('Validate password', () => {
  it('should return: false', (done) => {
      api.post('/api/password')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "asd"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(false);
              done();
          });
  });
})

describe('Validate email', () => {
  it('should return: true', (done) => {
      api.post('/api/email')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "henori.istheproblem@gmail.com"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(true);
              done();
          });
  });
})

describe('Validate email', () => {
  it('should return: false', (done) => {
      api.post('/api/email')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "henori.istheproblem_/asd-gmail.com"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(false);
              done();
          });
  });
})

describe('Validate birthday', () => {
  it('should return: true', (done) => {
      api.post('/api/birthday')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "05/07/1991"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(true);
              done();
          });
  });
})

describe('Validate birthday', () => {
  it('should return: false', (done) => {
      api.post('/api/birthday')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "05/07/199145"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(false);
              done();
          });
  });
})

describe('Validate username', () => {
  it('should return: true', (done) => {
      api.post('/api/username')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "kayttaja123"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(true);
              done();
          });
  });
})

describe('Validate username', () => {
  it('should return: false', (done) => {
      api.post('/api/username')
          .set('Connection', 'keep alive')
          .set('Content-Type', 'application/json')
          .type('form')
          .send({
              string: "kayttaj@/"
          })
          .end((err, res) => {
              res.status.should.equal(200);
              res.body.result.should.equal(false);
              done();
          });
  });
})
