/** 
 * Gulpfile module
 * @module Gulpfile
 */

/** Import gulp modules */
const gulp = require('gulp')
const jsdoc = require('gulp-jsdoc3')
const mocha = require('gulp-mocha');

/** 
 * Generate documentation
 * @param {function} cb - Callback function
 */
function docs(cb) {
  gulp.src(['./test.js', './index.js', './gulpfile.js', './public/**/*.js', './validations.js'], {read: false})
    .pipe(jsdoc(cb))
}

/**
 * Generate test reports
 * @param {function} cb - Callback function
 */
function testReport(cb) {
  gulp.src('test.js', {read: false})
    .pipe(mocha({reporter: 'mochawesome', exit: true}))
  cb()
}

/** Export gulp tasks */
exports.default = gulp.series(docs, testReport)
exports.docs = docs
exports.testreport = testReport