/** Import modules */
const express =  require('express');
const bodyParser = require('body-parser');
const validate = require('./validations');
const cors = require('cors')
const path = require('path')

/** Define constants */
const { urlencoded, json } = bodyParser;
const port = process.env.PORT || 8080;
const app = express();

/** Parse incoming requests data 
 * (https://github.com/expressjs/body-parser)
 */
app.use(json());
app.use(urlencoded({ extended: false }));
/** Use a CORS middleware */
app.use(cors())
/** Serve static files from public folder */
app.use(express.static('public'))
/** Serve documentation files */
app.use('/docs/gen/', express.static(__dirname + '/docs/gen/'));
/** Serve test report files */
app.use('/mochawesome-report/', express.static(__dirname + '/mochawesome-report/'));


/**
 * API Endpoints
 */

/** Password validation endpoint */
app.post('/api/password', (req, res) => {
  const { string } = req.body;
  const result =  validate.password(string);
  return res.status(200).send({
    result
  });
});

/** Email validation endpoint */
app.post('/api/email', (req, res) => {
  const { string } = req.body;
  const result =  validate.email(string);
  return res.status(200).send({
    result
  });
});

/** Name validation endpoint */
app.post('/api/name', (req, res) => {
  const { string } = req.body;
  const result =  validate.name(string);
  return res.status(200).send({
    result
  });
});

/** Username validation endpoint */
app.post('/api/username', (req, res) => {
    const { string } = req.body;
    const result = validate.username(string);
    return res.status(200).send({
        result
    });
});

/** Birthday validation endpoint */
app.post('/api/birthday', (req, res) => {
    const { string } = req.body;
    const result = validate.birthday(string);
    return res.status(200).send({
        result
    });
});

/** App entry point */
app.get('/', (req, res) => res.status(200).send({
  message: 'Welcome to our glorious app',
}));

/** 404 response */
app.get('*', (req, res) => res.status(200).send({
  message: 'Nothing found here',
}));

/** Listen to the defined port */
app.listen(port, (err) => {
  if (!err) {
     console.log(`App started on port ${port}`);
  } else {
    console.log(err);
  }
});

module.exports = app;
