# Repolainen

An online query validator

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* [Git](https://git-scm.com/) -  Open source distributed version control system
* [NodeJS](https://nodejs.org/en/) - JavaScript runtime built on Chrome's V8 JavaScript engine

### Installing

Clone the repository from GitLab

```
git clone https://gitlab.com/my-awsume-group/repolainen.git
```

Change the working directory into the cloned repository

```
cd repolainen
```

Run `npm install` to install all the dependencies

```
npm install
```

Generate documentation and test reports by running the default gulp tasks

```
gulp
```

## Built with

* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [Mocha](https://mochajs.org/) - Feature-rich JavaScript test framework running on Node.js
* [Chai](https://www.chaijs.com/) - Assertion library for Node.js

## Authors

See the list of [contributors](https://gitlab.com/my-awsume-group/repolainen/-/graphs/master) who participated in this project

## Acknowledgments

* [McDavid Emereuwa](https://medium.com/@McDave95) - [Continuous Integration with Node.js, Heroku and GitLab CI/CD](https://medium.com/@McDave95/continuous-integration-with-node-js-heroku-and-gitlab-ci-cd-part-1-1fe93a1d5967)