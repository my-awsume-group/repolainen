

/**
 * AJAX POST request
 * @param {object} inputElem - The HTML input element object
 * @param {string} dataType - Used as the API endpoint name
 */
function request(inputElem, dataType) {
  let response
  $.ajax({
    type: "POST",
    url: `${window.location.origin}/api/${dataType}/`,
    data: {
      string: inputElem.val()
    },
    success: (res) => {
      response = res
    }
  }).done(() => {
    if (response.result) {
      inputElem.addClass('is-valid')
    } else if (!response.result) {
      inputElem.addClass('is-invalid')
    }
  })
}

/**
 * Validate-button event handler
 */
$('.validate-button').on('click', (e) => {
  let button = $(e.currentTarget)
  let dataType = button.data("type")
  let inputElem = $(`input[name="${dataType}"]`)
  request(inputElem, dataType)
  /** Clear */
  inputElem.on('change keypress', (e) => {
    /** Remove box-shadow */
    inputElem.removeClass('is-valid is-invalid')
    /** Remove event listeners */
    inputElem.unbind()
  })
})

$('.clear-button').on('click', (e) => {
  let button = $(e.currentTarget)
  let dataType = button.data("type")
  let inputElem = $(`input[name="${dataType}"]`)
  inputElem.removeClass('is-valid is-invalid')
  inputElem.val("")
})