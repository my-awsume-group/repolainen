const CACHE_NAME = 'static-cache-v1'
const CACHE_ARR = [
  '/offline.html',
  '/assets/images/logo.png',
  '/assets/scripts/app.js',
  '/assets/styles/style.css'
]

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  // Precache static resources
  evt.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page')
      return cache.addAll(CACHE_ARR)
    })
  )
  self.skipWaiting();
})

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // Remove previous cached data from disk
  evt.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key !== CACHE_NAME) {
          console.log('[ServiceWorker] Removing old cache', key)
          return caches.delete(key)
        }
      }));
    })
  );
  self.clients.claim();
})

self.addEventListener('fetch', (evt) => {
  console.log('[ServiceWorker] Fetch', evt.request.url);
  if (evt.request.mode !== 'navigate') {
    // Not a page navigation, bail.
    return
  }
  evt.respondWith(
      fetch(evt.request)
          .catch(() => {
            return caches.open(CACHE_NAME)
                .then((cache) => {
                  return cache.match('offline.html')
                })
          })
  )
})

