/** 
 * Validation module
 * @module Validations
 */
/**
 * Validate string against a regular expression
 * @param {object} regEx - Regular expression object
 * @param {string} string - String to be validated
 */
function validator(regEx, string) {
  if (regEx.test(string)) {
    return true
  } else {
    return false
  }
}

/** 
 * Exports
 */
module.exports = {
  /**
   * Password validation
   * @param {string} string - Password string
   */
  password: (string) => {
    let regEx = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm)
    return validator(regEx, string)
  },
  /**
   * Email validation
   * @param {string} string - Email string
   */
  email: (string) => {
    let regEx = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gm)
    return validator(regEx, string)
  },
  /**
   * Name validation
   * @param {string} string - Name string
   */
  name: (string)=>{
    let regEx = new RegExp(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/gm)
    return validator(regEx, string)
  },
  /**
   * Username validation
   * @param {string} string - Username string
   */
  username: (string) => {
    let regEx = new RegExp(/^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/gm)
    return validator(regEx, string)
  },
  /**
   * Birthday validation
   * @param {string} string - Birthday string
   */
  birthday: (string) => {
    let regEx = new RegExp(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/gm)
    return validator(regEx, string)
  }
}
